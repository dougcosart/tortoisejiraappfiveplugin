namespace TortoiseJiraAppFivePlugin
{
    public class TicketItem
    {
        private readonly string _ticketID;
        private readonly string _ticketSummary;

        public TicketItem(string ticketID, string ticketSummary)
        {
            _ticketID = ticketID;
            _ticketSummary = ticketSummary;
        }

        public string ID
        {
            get { return _ticketID; }
        }

        public string Summary
        {
            get { return _ticketSummary; }
        }
    }
}