﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Atlassian.Jira;
using Interop.BugTraqProvider;

namespace TortoiseJiraAppFivePlugin
{
	[ComVisible(true),
        Guid("7B08A06D-1A45-406E-B277-369A6ECDADCD"),
        ClassInterface(ClassInterfaceType.None)]
    public class MyPlugin : IBugTraqProvider2, IBugTraqProvider
    {
        private List<TicketItem> selectedTickets = new List<TicketItem>();
		private Dictionary<string, string> parameterValues = new Dictionary<string, string>();

		public MyPlugin()
		{
#if DEBUG
			System.Diagnostics.Debugger.Launch();
#endif
		}

		bool IBugTraqProvider.ValidateParameters(IntPtr hParentWnd, string parameters)
		{
			return ValidateParameters(hParentWnd, parameters);
		}

		string IBugTraqProvider.GetLinkText(IntPtr hParentWnd, string parameters)
		{
			return GetLinkText();
		}

		string IBugTraqProvider.GetCommitMessage(IntPtr hParentWnd, string parameters, string commonRoot, string[] pathList,
									   string originalMessage)
		{
			string dummystring = "";

			return GetCommitMessage2(hParentWnd, parameters, "", commonRoot, pathList, originalMessage, "", out dummystring);
		}

		bool IBugTraqProvider2.ValidateParameters(IntPtr hParentWnd, string parameters)
        {
			return ValidateParameters(hParentWnd, parameters);
		}

		string IBugTraqProvider2.GetLinkText(IntPtr hParentWnd, string parameters)
        {
            return GetLinkText();
        }

        string IBugTraqProvider2.GetCommitMessage(IntPtr hParentWnd, string parameters, string commonRoot, string[] pathList,
                                       string originalMessage)
        {
			string dummystring = "";

			return GetCommitMessage2(hParentWnd, parameters, "", commonRoot, pathList, originalMessage, "", out dummystring);
		}

		string IBugTraqProvider2.GetCommitMessage2 (IntPtr hParentWnd, string parameters, string commonURL, string commonRoot, string[] pathList,
							   string originalMessage, string bugID, out string bugIDOut, out string[] revPropNames, out string[] revPropValues )
		{
			/* Please note: revPropNames and revPropValues are ignored by TortoiseGit! However, you must return at least empty arrays */
			revPropNames = new string[2];
			revPropValues = new string[2];
			revPropNames[0] = "bugtraq:issueIDs";
			revPropNames[1] = "myownproperty";
			revPropValues[0] = "13, 16, 17";
			revPropValues[1] = "myownvalue";

			return GetCommitMessage2(hParentWnd, parameters, commonURL, commonRoot, pathList, originalMessage, bugID, out bugIDOut);
		}

		private Jira jiraClient;
		private Jira JiraClient
		{
			get
			{
				if (jiraClient == null)
				{
					System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
					try
					{
						jiraClient = Jira.CreateRestClient(@"https://tescan.atlassian.net", "doug.cosart@tescan.com", parameterValues[API_TOKEN_KEY]);
						jiraClient.MaxIssuesPerRequest = 100;
					}
					catch (Exception e)
					{
						throw new ArgumentException(string.Format("Error initializing Jira REST client: \n {0}", e.Message));
					}
				}
				return jiraClient;
			}
		}

		private bool ValidateParameters(IntPtr hParentWnd, string parameters)
		{
			return InitializeParameterValues(parameters);
		}


		private string GetLinkText()
		{
			return "Jira Issue";
		} 

		private string GetCommitMessage2( IntPtr hParentWnd, string parameters, string commonURL, string commonRoot, string[] pathList,
                               string originalMessage, string bugID, out string bugIDOut)
        {
			try
			{
				bugIDOut = bugID;

				if (!InitializeParameterValues(parameters))
				{
					throw new ArgumentException(string.Format("Invalid parameter"));
				}

				var issues = GetJiraOpenIssues();
				List<TicketItem> tickets = new List<TicketItem>();
				foreach (var issue in issues)
				{
					tickets.Add(new TicketItem(issue.Key.Value, issue.Summary));
				}

                MyIssuesForm form = new MyIssuesForm( tickets );
                if ( form.ShowDialog( ) != DialogResult.OK )
                    return originalMessage;

                StringBuilder result = new StringBuilder( originalMessage );
                if ( originalMessage.Length != 0 && !originalMessage.EndsWith( "\n" ) )
                    result.AppendLine( );

				if (form.TicketsFixed.Count() > 0)
				{
					bugIDOut = string.Empty;
					var isFirstBug = true;
					foreach (TicketItem ticket in form.TicketsFixed)
					{
						result.AppendFormat("{0}: {1}", ticket.ID, ticket.Summary);
						result.AppendLine();
						selectedTickets.Add(ticket);
						var bugNumer = Regex.Match(ticket.ID, "\\d+").Value;
						bugIDOut += isFirstBug ? bugNumer : string.Format(",{0}", bugNumer);
						isFirstBug = false;
					}
				}

                return result.ToString( );
            }
            catch ( Exception ex )
            {
                MessageBox.Show( ex.ToString( ) );
                throw;
            }
        }

        string IBugTraqProvider2.CheckCommit( IntPtr hParentWnd, string parameters, string commonURL, string commonRoot, string[] pathList, string commitMessage )
        {
            return string.Empty; // empty string indicates no errors
        }

        string IBugTraqProvider2.OnCommitFinished( IntPtr hParentWnd, string commonRoot, string[] pathList, string logMessage, int revision )
        {
			return string.Empty; // empty string indicates no errors
		}

        bool IBugTraqProvider2.HasOptions()
        {
            return true;
        }

        string IBugTraqProvider2.ShowOptionsDialog( IntPtr hParentWnd, string parameters )
        {
            OptionsForm form = new OptionsForm( );
			if (parameterValues.Count == 0)
			{
				try
				{
					ParseParameters(parameters);
				}
				catch (ArgumentException e) { } // do nothing, start with empty parameter values
			}

			var requiredParameterCount = parameterKeys.Length;
			if (parameterValues.Count == requiredParameterCount)
			{
				form.textBoxApiToken.Text = parameterValues[API_TOKEN_KEY];
				form.textBoxProjectKey.Text = parameterValues[PROJECT_KEY_KEY];
				form.textBoxAccountId.Text = parameterValues[USER_ACCOUNT_ID_KEY];
			}

			var resultParameters = parameters;
			if (form.ShowDialog() == DialogResult.OK)
			{
				var apiKey = form.textBoxApiToken.Text;
				var projectKey = form.textBoxProjectKey.Text;
				var accountId = form.textBoxAccountId.Text;
				if (!string.IsNullOrWhiteSpace(apiKey) && !string.IsNullOrWhiteSpace(projectKey) && !string.IsNullOrWhiteSpace(accountId))
				{
					resultParameters = string.Format("{0}={1},{2}={3},{4}={5}",API_TOKEN_KEY, form.textBoxApiToken.Text, PROJECT_KEY_KEY, form.textBoxProjectKey.Text, USER_ACCOUNT_ID_KEY, form.textBoxAccountId.Text);
				}
			}
			return resultParameters;
        }

		//private List<TicketItem> GetJiraOpenIssues(string projectId, string user)
		private IEnumerable<Issue> GetJiraOpenIssues()
		{
			//var jira = Jira.CreateRestClient(@"https://tescantempe.atlassian.net", "doug@appfive.com", "1ffbNlu0M3qZ0ymy1poW483E");

			string jqlQuery = GetJqlIssueQuery(parameterValues[PROJECT_KEY_KEY], parameterValues[USER_ACCOUNT_ID_KEY]);
			return JiraClient.GetIssuesFromJql(jqlQuery);
		}

		private const string API_TOKEN_KEY = "apitoken";
		private const string USER_ACCOUNT_ID_KEY = "userid";
		private const string PROJECT_KEY_KEY = "projectkey";
		private readonly string[] parameterKeys = { API_TOKEN_KEY, USER_ACCOUNT_ID_KEY, PROJECT_KEY_KEY };
		private bool InitializeParameterValues(string parameters)
		{
			try
			{
				ParseParameters(parameters);
				ValidateProjectKey(parameterValues[PROJECT_KEY_KEY]);
				// 2020-04-16 ValidateUserAccount uses Atlassian.Jira.GetUserAsync(), which seems to be broken. 
				// It looks like under the hood this call queries for username, which is no longer supported
				// This call should work fine when the Jira SDK is updated to query with accountid instead of username
				//ValidateUserAccount(parameterValues[USER_ACCOUNT_ID_KEY]);
			}
			catch (ArgumentException e)
			{
				System.Windows.Forms.MessageBox.Show(e.Message, "Parameter Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
			return true;
		}

		private readonly string PARAMETER_EXAMPLE_MESSAGE = string.Format("Example: '{0}=abcdef012345678901234567,{1}=DEV,{2}=12345-6789-0001'", API_TOKEN_KEY, PROJECT_KEY_KEY, USER_ACCOUNT_ID_KEY);
		private void ParseParameters(string parameters)
		{
			var requiredParameterCount = parameterKeys.Length;
			if (parameterValues.Count == 0)
			{
				var keyValueStrings = parameters.Split(',');
				if (keyValueStrings.Length == requiredParameterCount)
				{
					foreach (var keyValueString in keyValueStrings)
					{
						var keyAndValueStrings = keyValueString.Split('=');
						if (keyAndValueStrings.Length == 2)
						{
							parameterValues.Add(keyAndValueStrings[0].ToLower(), keyAndValueStrings[1]);
						}
						else
						{
							parameterValues.Clear();
							break;
						}
					}
				}
				if (parameterValues.Count == requiredParameterCount)
				{
					foreach (var parameterKey in parameterKeys)
					{
						if (!parameterValues.ContainsKey(parameterKey))
						{
							parameterValues.Clear();
							break;
						}
					}
				}
			}
			if (parameterValues.Count != requiredParameterCount)
			{
				var PARAMETER_ERROR_MESSAGE = string.Format("'{0}', '{1}', and '{2}' parameters required, \n{2}", API_TOKEN_KEY, PROJECT_KEY_KEY, USER_ACCOUNT_ID_KEY, PARAMETER_EXAMPLE_MESSAGE);
				throw new ArgumentException(PARAMETER_ERROR_MESSAGE);
			}
		}

		private void ValidateProjectKey(string projectKey)
		{
			var projects = JiraClient.GetProjects();
			var found = false;
			foreach (var project in projects)
			{
				found = project.Key == projectKey;
				if (found)
				{
					break;
				}
			}
			if (!found)
			{
				throw new ArgumentException(string.Format("Project key '{0}' not found in Jira", projectKey));
			}
		}

		private void ValidateUserAccount(string userAccountId)
		{
			try
			{
				Task<Atlassian.Jira.JiraUser> j = JiraClient.GetUserAsync(userAccountId);
				string h = j.Result.DisplayName;
			}
			catch (Exception e)
			{
				throw new ArgumentException(string.Format("Error validating user account: {0}\n{1}", userAccountId, e.Message));
			}
		}

		private string GetJqlIssueQuery(string projectKey, string userAccountId)
		{
			return string.Format("project={0}&assignee={1}&resolution=unresolved&sprint!=EMPTY&(status=merged OR status=completed OR status=doing OR status='sprint backlog')", projectKey, userAccountId);
		}

    }
}
