﻿namespace TortoiseJiraAppFivePlugin
{
    partial class OptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.textBoxProjectKey = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.textBoxAccountId = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.textBoxApiToken = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.button1.Location = new System.Drawing.Point(114, 229);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 2;
			this.button1.Text = "OK";
			this.button1.UseVisualStyleBackColor = true;
			// 
			// button2
			// 
			this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.button2.Location = new System.Drawing.Point(207, 229);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 3;
			this.button2.Text = "Cancel";
			this.button2.UseVisualStyleBackColor = true;
			// 
			// textBoxProjectKey
			// 
			this.textBoxProjectKey.Location = new System.Drawing.Point(81, 55);
			this.textBoxProjectKey.Name = "textBoxProjectKey";
			this.textBoxProjectKey.Size = new System.Drawing.Size(288, 20);
			this.textBoxProjectKey.TabIndex = 4;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 57);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(61, 13);
			this.label1.TabIndex = 5;
			this.label1.Text = "Project Key";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 83);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(61, 13);
			this.label2.TabIndex = 7;
			this.label2.Text = "Account ID";
			// 
			// textBoxAccountId
			// 
			this.textBoxAccountId.Location = new System.Drawing.Point(81, 81);
			this.textBoxAccountId.Name = "textBoxAccountId";
			this.textBoxAccountId.Size = new System.Drawing.Size(288, 20);
			this.textBoxAccountId.TabIndex = 6;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 31);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(58, 13);
			this.label3.TabIndex = 9;
			this.label3.Text = "API Token";
			// 
			// textBoxApiToken
			// 
			this.textBoxApiToken.Location = new System.Drawing.Point(81, 29);
			this.textBoxApiToken.Name = "textBoxApiToken";
			this.textBoxApiToken.Size = new System.Drawing.Size(288, 20);
			this.textBoxApiToken.TabIndex = 8;
			// 
			// OptionsForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(389, 264);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.textBoxApiToken);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.textBoxAccountId);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.textBoxProjectKey);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Name = "OptionsForm";
			this.Text = "Tortoise Jira AppFive Options";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
		public System.Windows.Forms.TextBox textBoxProjectKey;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		public System.Windows.Forms.TextBox textBoxAccountId;
		private System.Windows.Forms.Label label3;
		public System.Windows.Forms.TextBox textBoxApiToken;
	}
}